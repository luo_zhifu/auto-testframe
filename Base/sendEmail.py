import smtplib
from Base import baseinfo
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.mime.text import MIMEText
from Base.parser_time import *

#Demo测试
def send_Mail(file_name):

    f = open(file_name, 'rb')
    # 读取测试报告正文
    mail_body = f.read()
    f.close()
    try:
        smtp = smtplib.SMTP(baseinfo.Smtp_Server, 25)
        sender = baseinfo.Smtp_Sender
        password = baseinfo.Smtp_Sender_Password
        receiver = baseinfo.Smtp_Receiver
        smtp.login(sender, password)
        msg = MIMEMultipart()
        contentText = strftime()+'自动化测试报告'
        text = MIMEText(contentText, 'plain', 'utf-8')
        msg.attach(text)

        now = strftime_ymd()
        msg['Subject'] = Header('[ '+ now+ ' ]'+ 'UI自动化测试报告', 'utf-8')
        msg_file = MIMEText(mail_body, 'base64', 'utf-8')
        msg_file['Content-Type'] = 'application/octet-stream'
        msg_file["Content-Disposition"] = 'attachment; filename="TestReport.docx"'
        msg.attach(msg_file)

        msg['From'] = sender

        msg['To'] = ",".join(receiver)
        tmp = smtp.sendmail(sender, receiver, msg.as_string())
        print ('发送邮件成功!')
        smtp.quit()
        return True
    except smtplib.SMTPException as e:
        print('发送邮件失败!')
        print(str(e))
        return False

