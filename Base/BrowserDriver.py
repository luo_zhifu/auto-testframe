from selenium import webdriver
from Base.logger import Logger
from Base.var import *
import os
import gc
from ruamel import yaml
from Base.parser_excel import ParserExcel
from Base.reuseBrowser import ReuseBrowser

logger = Logger(logger="BrowserDriver").getlog()

class BrowserDriver(object):
    curpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    chrome_driver_path = os.path.join(curpath, r"driver\chromedriver.exe")
    ie_driver_path = os.path.join(curpath, r"driver\IEDriverServer.exe")
    firefoxDriverPath =  os.path.join(curpath,r"driver\geckodriver.exe")

    def __init__(self,driver):
        self.driver = driver

    # 应对测试中断场景：如果测试过程突然中断，则标记为取消状态的用例------------------
    def caseCancle(self,pathEexcel):
        excel = ParserExcel(pathEexcel, statue=False)
        excel.set_sheet_by_name('Case')
        caseSheetMaxR = excel.get_max_row()
        caseSheetMaxC = excel.get_max_column()
        # 指定行的列
        appoint_colnum = caseSheetMaxC - 4
        # 用例起始行的列
        start_colnum = caseSheetMaxC - 3
        # 用例结束行的列
        finish_colnum = caseSheetMaxC - 2
        for row in range(3, caseSheetMaxR + 1):
            excel.set_sheet_by_name('Case')
            caseName = excel.get_cell_value(row, 3)
            swithValue = excel.get_cell_value(row, 1)
            # 获取指定行、开始行、结束行的数据
            appoint_rowNum = excel.get_cell_value(row, appoint_colnum)
            start_rowNum = excel.get_cell_value(row, start_colnum)
            finish_rowNum = excel.get_cell_value(row, finish_colnum)
            # 去掉用例模块第一列、时间列、结果列背景颜色
            excel.set_cellColor(row, 1, 'FFFFFF')
            excel.set_cellColor(row, caseSheetMaxC, 'FFFFFF')
            excel.set_cellColor(row, caseSheetMaxC - 1, 'FFFFFF')
            # 判断用例数据执行-------------------------------------------------------------------------
            if swithValue == 'Y':
                # 初始用例结果状态为取消，时间为空
                excel.set_cell_value(row, caseSheetMaxC, 'Cancled')
                excel.set_cellColor(row, caseSheetMaxC, 'FF7F00')
                excel.set_cell_value(row, caseSheetMaxC - 1, '')
                excel.set_sheet_by_name(caseName)
                caseMaxR = excel.get_max_row()
                caseMaxC = excel.get_max_column()
                # 指定行时
                if appoint_rowNum:
                    start_rowNum = appoint_rowNum + 6
                    finish_rowNum = appoint_rowNum + 6
                # 非指定行时
                else:
                    if start_rowNum is None:
                        start_rowNum = 7
                    else:
                        start_rowNum = start_rowNum + 6
                    if finish_rowNum is None:
                        finish_rowNum = caseMaxR
                    else:
                        finish_rowNum = finish_rowNum + 6
                for caseRow2 in range(7, caseMaxR + 1):
                    if excel.get_cell_value(caseRow2, 1):
                        # 初始去掉时间和结果列颜色
                        excel.set_cellColor(caseRow2, caseMaxC - 2, 'FFFFFF')
                        excel.set_cellColor(caseRow2, caseMaxC - 3, 'FFFFFF')
                        # -----------------------------------------------------
                        # 初始报错列、截图列为空
                        excel.set_cell_value(caseRow2, caseMaxC, '')
                        excel.set_cell_value(caseRow2, caseMaxC - 1, '')
                for caseRow1 in range(start_rowNum, finish_rowNum + 1):
                    actionOrnot = excel.get_cell_value(caseRow1, 3)
                    # 序号
                    orderNum = excel.get_cell_value(caseRow1, 1)

                    # 初始如果不测试，结果列设置为空
                    if actionOrnot != 'N' and orderNum is not None :
                        # 设置时间为空
                        excel.set_cell_value(caseRow1, caseMaxC - 3, '')
                        # 初始如果开启测试，结果列为取消
                        excel.set_cell_value(caseRow1, caseMaxC - 2, 'Cancled')
                        excel.set_cellColor(caseRow1, caseMaxC - 2, 'FF7F00')
            else:
                continue
        #保存excel
        excel.save_excel()
        # 关闭excel进程，防止内存占用过大
        del excel
        gc.collect()

    #打开浏览器、输入URL的操作---------------------------------------------------
    def openbrowser(self,pathExcel):
        #先调用初始化处理excel
        self.caseCancle(pathExcel)

        #再读取参数操作浏览器
        excel = ParserExcel(pathExcel)
        # 参数sheet
        excel.set_sheet_by_name('Param')
        url= excel.get_cell_value(2, 4)
        browser_value = excel.get_cell_value(2, 3)
        browser_value = browser_value.lower()

        if browser_value == "firefox":
            # profile_directory = r'C:\Users\39330\AppData\Roaming\Mozilla\Firefox\Profiles\fgrvgref.default'
            # # 加载火狐配置文件，目的让火狐加载插件，方便调试
            # profile = webdriver.FirefoxProfile(profile_directory)
            self.driver = webdriver.Firefox(executable_path=self.firefoxDriverPath)
            logger.info("打开新的浏览器:火狐浏览器...")
            # 隐式等待10s
            self.driver.implicitly_wait(10)
            self.driver.get(url)
            logger.info("打开URL: %s" % url)
            self.driver.maximize_window()
            logger.info("全屏当前窗口")
            self.driver.implicitly_wait(5)
            logger.info("设置5秒隐式等待时间")
        else:
            try:
                #读取yaml文件数据
                curpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
                yaml_path = os.path.join(curpath, r"yaml\reuseBrowser.yaml")
                readData = open(yaml_path,"r")
                result = yaml.load(readData.read(),Loader=yaml.Loader)
                exe_url = result['executor_url']
                sessionID = result['session_id']
                #解决IO警告
                readData.close()
                #使用selenium已打开的浏览器
                self.driver = ReuseBrowser(command_executor=exe_url, session_id=sessionID)
                #主要用来触发异常
                except_flag = self.driver.current_url
                logger.info("上一次浏览器的URL为： %s" %except_flag)
                if except_flag:
                    logger.info("使用selenium上一次打开的浏览器:%s" % browser_value)
                    # driver.maximize_window()#该方法，使用谷歌浏览器会报错
                    # logger.info("全屏当前窗口...")
            except Exception:
                if browser_value == "chrome":
                    opt =webdriver.ChromeOptions()
                    opt.add_experimental_option('w3c',False)
                    # opt.add_argument("--user-data-dir="+r"C:\Users\Administrator\AppData\Local\Google\Chrome\User Data")
                    self.driver = webdriver.Chrome(self.chrome_driver_path,options=opt)
                    logger.info("打开新的浏览器:谷歌浏览器...")
                else:
                    print("test--------------------------")
                    self.driver = webdriver.Ie(self.ie_driver_path)
                    logger.info("打开新的浏览器:IE浏览器...")
                # 动态储存浏览器的远程服务连接和sessionid，作为使用已打开浏览器用途-----------------------------
                executor_url = self.driver.command_executor._url
                session_id = self.driver.session_id
                # 将数据以字典形式写入到yaml
                reuse_browser = {
                    'browser': browser_value,
                    'session_id': session_id,
                    'executor_url': executor_url

                }
                curpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
                yaml_path = os.path.join(curpath, r"yaml\reuseBrowser.yaml")
                with open(yaml_path, "w", encoding="utf-8") as f:
                    yaml.dump(reuse_browser, f, Dumper=yaml.RoundTripDumper)
                #----------------------------------------------------------------------------------------
                #隐式等待10s
                self.driver.implicitly_wait(10)
                self.driver.get(url)
                logger.info("打开URL: %s" % url)
                self.driver.maximize_window()
                logger.info("全屏当前窗口")
                self.driver.implicitly_wait(5)
                logger.info("设置5秒隐式等待时间")

        return self.driver

    def quit_browser(self):
        logger.info("关闭浏览器")
        self.driver.quit()

