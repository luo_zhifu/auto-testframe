import time
from Base.all_Action import Action
from Base.parser_excel import ParserExcel
import traceback
import datetime
from Base.logger import Logger
import gc
from Base.just_open_excel import ParserExcel_xlwing
from Base.create_dir import *
from Base.var import *
from Base.parser_time import *
from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH

import smtplib
from Base import baseinfo
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.mime.text import MIMEText


logger = Logger(logger='CommonFunc').getlog()
class CommonFunc(object):
    #初始化driver对象
    def __init__(self,driver):
        self.driver = driver

    #发送邮件的处理逻辑-----------------------------------------------------------------------
    def send_Mail(self,file_name,reportName):
        f = open(file_name, 'rb')
        # 读取测试报告正文
        mail_body = f.read()
        f.close()
        try:
            smtp = smtplib.SMTP(baseinfo.Smtp_Server, 25)
            sender = baseinfo.Smtp_Sender
            password = baseinfo.Smtp_Sender_Password
            receiver = baseinfo.Smtp_Receiver
            smtp.login(sender, password)
            msg = MIMEMultipart()
            contentText = strftime() + '自动化测试报告'
            text = MIMEText(contentText, 'plain', 'utf-8')
            msg.attach(text)

            now = strftime_ymd()
            msg['Subject'] = Header('[ ' + now + ' ]' + '自动化测试报告', 'utf-8')
            msg_file = MIMEText(mail_body, 'base64', 'utf-8')
            msg_file['Content-Type'] = 'application/octet-stream'
            #支持发送中文名称的附件
            msg_file.add_header("Content-Disposition", "attachment", filename=("gbk", "",reportName ))
            # msg_file["Content-Disposition"] = 'attachment; filename="TestReport.docx"'
            msg.attach(msg_file)

            msg['From'] = sender

            msg['To'] = ",".join(receiver)
            tmp = smtp.sendmail(sender, receiver, msg.as_string())
            logger.info('发送邮件成功!...')
            smtp.quit()
            return True
        except smtplib.SMTPException as e:
            logger.info('发送邮件失败!...')
            raise  e

    #用例的表执行前置、提交、开始、后续数据操作处理逻辑---------------------------------------------------
    def handCase(self,caseData,sheetName,actionName,hightLight=False):
        caseData = str(caseData).strip()
        logger.info("%s表执行%s:%s......" % (sheetName, actionName, caseData))
        actionValue = caseData.split(";\n")
        for action in actionValue:
            action_a = action[action.index('.') + 1:]
            action_d = action[:action.index('.')]
            action_b = action_a[:action_a.index('(')]
            if action_d=='take_action':
                if hightLight:
                    action_c = action.replace(action_b, 'ele_highLight')
                    eval(action_c)
                eval(action)
            else:
                eval(action)

    # 用例的列执行前置，后续操作处理逻辑----------------------------------------------------------------------
    def handColCase(self,caseData,actionName,funcName,orderNum,hightLight=False,value=None):
        caseData = str(caseData).strip()
        logger.info("执行%s:%s......" % (actionName, caseData))
        actionValue = caseData.split(";\n")
        captureName = []
        for action in actionValue:
            action_a = action[action.index('.') + 1:]
            action_d = action[:action.index('.')]
            #方法名
            action_b = action_a[:action_a.index('(')]
            strFlag = '","'
            result = strFlag in action
            if action_d=='take_action' and result is True:
                action_e = action[:-2]
                action_f = action_e[action_e.index(',') + 2:]
                if action_f[-1]=='=' and value is not None:
                    expressValue = action_f + "'" + str(value) + "']"
                    action_g = action.replace(action_f, expressValue)
                    if hightLight:
                        action_c = action_g.replace(action_b, 'ele_highLight')
                        eval(action_c)
                    eval(action_g)
                elif action_f[-1] == ',' and value is not None:
                    expressValue = action_f + "'" + str(value) + "')]"
                    action_g = action.replace(action_f, expressValue)
                    if hightLight:
                        action_c = action_g.replace(action_b, 'ele_highLight')
                        eval(action_c)
                    eval(action_g)
                else:
                    if hightLight:
                        action_c = action.replace(action_b, 'ele_highLight')
                        eval(action_c)
                    eval(action)
            elif action_b == 'capture_screen':
                ParamCapture = action[:action.index(')')]
                captureAction = ParamCapture + ',' +'"'+funcName+'_测试用例_'+str(orderNum) + '")'
                captureData = eval(captureAction)
                captureName.append({"captureData":captureData})
            else:
                eval(action)
        return captureName

    #写word报告文档逻辑----------------------------------------------------------------------------
    def writeDocReport(self,pathExcel,case_sumNum,doc_Explain):
        """写doc文档"""
        try:
            ExcelPath = pathExcel
            run_dir = os.path.split(ExcelPath)[0]
            nowTime = time.strftime("%Y-%m-%d_%H_%M_%S")
            DocxName = os.path.splitext(os.path.split(ExcelPath)[1])[0]
            RunExcelTime = str(nowTime)
            # 报告文件的路径
            FILE_PATH = os.path.dirname(run_dir) + '\\testResult\\' + DocxName +'\\'+strftime_ymd()+'\\'
            if not os.path.exists(FILE_PATH):
                os.makedirs(FILE_PATH)

            docx_dir = FILE_PATH + DocxName+"__Report_" + RunExcelTime + r'.docx'
            docxName = DocxName+"__Report_" + RunExcelTime + r'.docx'
            document = Document()
            p = document.add_heading(DocxName + " _Project", 0)  # 0标题
            p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER  # 文字居中显示
            p = document.add_heading("测试报告", 0)  # 0标题
            p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER  # 文字居中显示
            document.add_heading("测试执行整体详情:",1)
            document.add_paragraph('开始时间：' + case_sumNum[5]+'————————'+'结束时间：' + strftime())
            #分割时间，规范时间格式
            Tree_sumTime = str(case_sumNum[3]).split('.')
            document.add_paragraph('用例执行耗时：' +Tree_sumTime[0])
            document.add_paragraph('使用的浏览器：' + case_sumNum[4])
            table = document.add_table(rows=1, cols=3, style="Table Grid")
            hdr_cells = table.rows[0].cells
            hdr_cells[0].text = '执行用例总数'
            hdr_cells[1].text = '成功数'
            hdr_cells[2].text = '失败数'
            row_cells = table.add_row().cells
            row_cells[0].text = str(case_sumNum[0])
            row_cells[1].text = str(case_sumNum[1])
            row_cells[2].text = str(case_sumNum[2])
            document.add_heading("用例执行具体详情：",2)
            for docExplain in doc_Explain:
                #成功情况
                if docExplain['TestResult_pass'] is not None:
                    if docExplain['funcName'] is not None:
                        document.add_heading('测试模块：'+docExplain['funcName'],3)
                    if docExplain['doc_Explain_name'] is not None:
                        document.add_paragraph('用例详情：'+docExplain['doc_Explain_name'])
                    #如果有截图
                    if docExplain['capture_pass'] is not None:
                        document.add_paragraph('截图详细：'+docExplain['capture_pass'][1])
                        document.add_picture(docExplain['capture_pass'][0], width=Inches(6.0))
                #失败情况
                elif docExplain['TestResult_fail'] and docExplain['capture_fail'] is not None :
                    if docExplain['funcName'] is not None:
                        document.add_heading('测试模块：'+docExplain['funcName'],3)
                    document.add_paragraph('用例描述：'+docExplain['doc_Explain_name'])
                    document.add_paragraph('失败详情：'+docExplain['capture_fail'][2] )
                    document.add_picture(docExplain['capture_fail'][0], width=Inches(6.0))
                else:
                    break
            logger.info('生成word测试报告...')
            document.save(docx_dir)
            return docx_dir,docxName
        except Exception:
            raise

    #excel用例数据处理逻辑--------------------------------------------------------------------------
    def actionTestcase_V1(self,pathExcel,hightLight = False):
        #测试开始时间
        startTestime = strftime()
        #使用xlwing 打开和关闭操作，防止openpyxl无法连续执行同一个excel文档（即文档如果从没被打开过）
        excel_xlwing = ParserExcel_xlwing(pathExcel)
        excel_xlwing.save_excel()
        excel_xlwing.close_excel()
        #全局take_action对象
        global take_action
        take_action = Action(self.driver)
        case_name = {'Param': 'Param',
                     'Case' : 'Case',
                     'actionParam' : '前置后置操作'
                     }
        excel = ParserExcel(pathExcel)
        #参数sheet----------------------------------------------------------------------------------------
        excel.set_sheet_by_name(case_name['Param'])
        # 语言
        words = {
            'English': {'Success': 'Pass', 'Fail': 'Fail',},
            'Chinese': {'Success': '成功', 'Fail': '失败'}
        }
        # 参数列
        param = {'break': True,
                 'continue': False
                 }
        # 参数值
        param_value = excel.get_cell_value(2, 1)
        if param_value == None:
            param_value = 'break'
        select_param = param[param_value]
        # 语言值
        word_value = excel.get_cell_value(2, 2)
        if word_value == None:
            word_value = 'Chinese'
        select_words = words[word_value]
        #浏览器
        browserValue = excel.get_cell_value(2, 3)
        # 绿色，紫色，棕色，蓝色
        bgcolor = {
            "green": "00FF00",
            "purple": "BF3EFF",
            "M_red" : "FF00FF",
            "brown": "EEAD0E",
            "blue": "1E90FF"
        }
        # 执行背景颜色
        background_rowNum = excel.get_cell_value(2, 5)
        if background_rowNum == None:
            background_rowNum = 'green'
        select_color = bgcolor[background_rowNum]
        # 失败的背景颜色(红色)
        error_color = "FF0000"
        # 失败重试
        reload = excel.get_cell_value(1,7)
        #重试次数
        num = excel.get_cell_value(2,7)
        #发送邮件开关
        switchEmail = excel.get_cell_value(2,8)
        # 用例sheet-----------------------------------------------------------------------------------------
        excel.set_sheet_by_name(case_name['Case'])
        #指定行的列
        appoint_colnum = excel.get_max_column() - 4
        #用例起始行的列
        start_colnum = excel.get_max_column() - 3
        #用例结束行的列
        finish_colnum = excel.get_max_column() - 2
        #功能名称的列
        funcName_colnum = 3
        # 用例执行时间
        alltime_colnum = excel.get_max_column() - 1
        # 用例执行结果
        allresult_colnum = excel.get_max_column()

        if hightLight:
            logger.info("测试已开启高亮模式...")
        #循环用例sheet的行-----------------------------------------------------------------------------------------------------
        #标记用例失败和成功数
        passNum = 0
        failNum = 0
        doc_Explain = []
        startSum_time = datetime.datetime.now()
        for rownum in range(3, excel.get_max_row() + 1):
            #初始化excel对象
            excel = ParserExcel(pathExcel)
            excel.set_sheet_by_name(case_name['Case'])
            #标记用例模块行
            case_flag = True
            switch_value = excel.get_cell_value(rownum, 1)
            startAll_time = datetime.datetime.now()
            if switch_value == "Y":
                # 功能名称
                funcName = excel.get_cell_value(rownum,funcName_colnum)
                #获取指定行、开始行、结束行的数据
                appoint_rowNum = excel.get_cell_value(rownum, appoint_colnum)
                start_rowNum = excel.get_cell_value(rownum, start_colnum)
                finish_rowNum = excel.get_cell_value(rownum, finish_colnum)

                # 参数sheet
                excel.set_sheet_by_name(case_name['actionParam'])
                # 第二列的值
                preName = '前置操作'
                startName = '开始操作'
                submitName = '提交操作'
                endName = '后续操作'
                #第三列默认值
                preAction = None
                startAction = None
                submitAction = None
                endAction = None
                paramRowMax = excel.get_max_row()
                #读取参数值
                for paramRow in range(1, paramRowMax + 1):
                    sheetName = excel.get_cell_value(paramRow, 1)
                    # 功能名
                    if funcName == sheetName :
                        #第三列的值
                        preAction = excel.get_cell_value(paramRow, 3)
                        startAction  = excel.get_cell_value(paramRow + 1, 3)
                        submitAction  = excel.get_cell_value(paramRow + 2, 3)
                        endAction  = excel.get_cell_value(paramRow + 3, 3)
                        #读完值就终止循环
                        break
                    else:
                        continue
                # 根据功能名称读取sheet名----------------------------------------------------------------------------------
                excel.set_sheet_by_name(funcName)
                #判断用例数据执行--------------------------------------------------------------------------------------------------
                #指定行时
                if appoint_rowNum:
                    start_rowNum = appoint_rowNum+6
                    finish_rowNum = appoint_rowNum+6
                #非指定行时
                else:
                    if start_rowNum is None:
                        start_rowNum = 7
                    else:
                        start_rowNum = start_rowNum + 6
                    if finish_rowNum is None:
                        finish_rowNum = excel.get_max_row()
                    else:
                        finish_rowNum = finish_rowNum + 6
                #---------------------------------------------------------------------------------------
                time_colnum = excel.get_max_column() - 3
                result_colnum = excel.get_max_column() - 2
                exception_colnum = excel.get_max_column() - 1
                screent_colnum = excel.get_max_column()
                # 循环用例列数
                colnum_num = excel.get_max_column() - 3
                #用来装截图信息
                capture_pass = []
                #标记一个模块下执行多个用例时不会重复写入funcName
                functionOnce = 0
                # 如果存在前置操作
                if preAction:
                    self.handCase(preAction,sheetName,preName,hightLight)
                else:
                    logger.info("%s表没有执行%s......" % (funcName, preName))

                for row in range(start_rowNum, finish_rowNum +1):
                    # 循环每行花费的时间
                    start_time = datetime.datetime.now()
                    #每行保存后的初始化excel对象
                    excel = ParserExcel(pathExcel)
                    excel.set_sheet_by_name(funcName)
                    # 执行的每个单元格
                    Data_Action = []
                    # 失败的信息
                    Data_Action_Fail = []
                    #每次写入完一行的截图，第二行进行清空重新写入
                    capture_pass.clear()
                    #标记用例数据行
                    global flag
                    flag = True

                    #用例开关
                    switch_value = excel.get_cell_value(row, 3)
                    #序号
                    orderNum = excel.get_cell_value(row, 1)
                    #说明
                    explain = excel.get_cell_value(row, 2)

                    #写入文档的说明
                    if switch_value == 'N':
                        continue
                    elif orderNum is None:
                        continue
                    else:
                        if explain is None:
                            explain = '用例无说明'
                        # 如果存在开始操作
                        if startAction:
                            self.handCase(startAction, sheetName, startName, hightLight)
                        else:
                            logger.info("%s表没有执行%s......" % (funcName, startName))
                        #------------------------------------------------------------------------------------
                        for colnum in range(4, colnum_num):
                            data = []
                            errorData = []
                            #字符串value多个组合的时候
                            SumValue = []
                            #获取测试步骤备注
                            TestPlain = excel.get_cell_value(6,colnum)
                            #方法
                            action = excel.get_cell_value(1, colnum)
                            #定位类型
                            local_type = excel.get_cell_value(2, colnum)

                            if local_type == None:
                                local_type='xpath'
                            local_type = str(local_type).strip()
                            data.append(local_type)
                            local_expression = excel.get_cell_value(3, colnum)
                            #列前置操作
                            columnStart = excel.get_cell_value(4, colnum)
                            columnStart_name = '列前置操作'
                            #列后续操作
                            columnEnd= excel.get_cell_value(5, colnum)
                            columnend_name = '列后续操作'
                            value = excel.get_cell_value(row, colnum)
                            #-------------------------------------------------------------------------------------
                            try:
                                if action is not None and value is not None:
                                    action = str(action).strip()
                                    value = str(value).strip()
                                    if local_expression is not None:
                                        local_expression = str(local_expression).strip()
                                        if local_expression[-1] == '=':
                                            newValue = str(value).split(";")
                                            for paramValue in newValue:
                                                newExpression = local_expression + "'" + str(paramValue)+"']"
                                                SumValue.append(newExpression)
                                            data.append(SumValue)
                                        elif  local_expression[-1] == ',':
                                            newValue = str(value).split(";")
                                            for paramValue in newValue:
                                                newExpression = local_expression + "'" + str(paramValue)+"')]"
                                                SumValue.append(newExpression)
                                            data.append(SumValue)
                                        elif  local_expression[-1] == ')':
                                            newValue = str(value).split(";")
                                            for paramValue in newValue:
                                                newExpression = local_expression + ",'" + str(paramValue)+"')]/../label"
                                                SumValue.append(newExpression)
                                            data.append(SumValue)


                                        else:
                                            data.append(local_expression)
                                    if value!='Y':
                                        #如果一次多个value参数
                                        data.append(value)
                                    #判断执行列前置操作
                                    if columnStart :
                                        if value != 'Y':
                                            captureData = self.handColCase(columnStart,columnStart_name,funcName,orderNum,hightLight,value)
                                            if len(captureData):
                                                for capturedata in captureData:
                                                    capture_pass.append({'capture_name': capturedata['captureData']})
                                            else:
                                                pass
                                        else:
                                            captureData = self.handColCase(columnStart, columnStart_name, funcName,orderNum,hightLight)
                                            if len(captureData):
                                                for capturedata in captureData:
                                                    capture_pass.append({'capture_name': capturedata['captureData']})
                                            else:
                                                pass
                                    if action=='capture_screen':
                                        data.clear()
                                        data.append(value)
                                        caseStation = funcName+"_测试用例_"+str(orderNum)
                                        data.append(caseStation)
                                        capture_name= getattr(take_action, 'capture_screen')(*data)
                                        capture_pass.append({'capture_name': capture_name})
                                    else:
                                        if hightLight :
                                            if local_expression:
                                                if len(SumValue)>0:
                                                    getattr(take_action, 'ele_highLightSpilt')(*data)
                                                else:
                                                    getattr(take_action, 'ele_highLight')(*data)
                                                    getattr(take_action, action)(*data)
                                        else:
                                            getattr(take_action, action)(*data)
                                    #储存执行了的列号
                                    Data_Action.append({
                                                        'Colnum': colnum
                                                        })
                                    #判断执行列后续操作
                                    if columnEnd:
                                        if value != 'Y':
                                            captureData = self.handColCase(columnEnd,columnend_name,funcName,orderNum,hightLight,value)
                                            if len(captureData):
                                                for capturedata in captureData:
                                                    capture_pass.append({'capture_name': capturedata['captureData']})
                                            else:
                                                pass
                                        else:
                                            captureData = self.handColCase(columnEnd, columnend_name, funcName,orderNum,hightLight)
                                            if len(captureData):
                                                for capturedata in captureData:
                                                    capture_pass.append({'capture_name': capturedata['captureData']})
                                            else:
                                                pass
                                else:
                                    # logger.info('第 %s 行 %s 列 跳过了' % (row, colnum))
                                    continue
                            #--------------------------------------------------------------------------------
                            except Exception:
                                reload_flag = False
                                #失败重试
                                if reload=='Y':
                                    logger.info("靓仔...! 有异常啊！下面开始%s次的重试..." %num)
                                    for i in range(1,num+1):
                                        try:
                                            if hightLight:
                                                if local_expression:
                                                     getattr(take_action, 'ele_highLight')(*data)
                                                getattr(take_action, action)(*data)
                                            else:
                                                getattr(take_action, action)(*data)

                                            Data_Action.append({'funcName': funcName,
                                                                'Row': row,
                                                                'Colnum': colnum,
                                                                })
                                            #标记重试后，如果成功，则无需记录异常信息
                                            reload_flag = True
                                            logger.info("靓仔！很好...重试第 %d 次成功了！" % i)
                                            break
                                        except Exception:
                                            logger.info("很遗憾...重试第 %d 次失败了！"% i)
                                            reload_flag = False
                                #--------------------------------------------------------------------------------
                                if reload_flag == False:
                                    # 记录异常信息
                                    global except_message
                                    except_message = traceback.format_exc()
                                    logger.info(except_message)
                                    if action == 'assert_word':
                                        except_message = "断言失败"
                                        logger.info("断言失败！")
                                    global result
                                    #errorData为截图名称提供使用
                                    errorData.append(str(funcName))
                                    errorData.append(str(row))
                                    errorData.append(str(colnum))
                                    errorData.append(str(TestPlain))
                                    result = take_action.capture_error_screen(*errorData)
                                    #储存出错列号
                                    Data_Action_Fail.append(colnum)
                                    #用来标记单条用例的成功与失败
                                    flag = False
                                    #用来标记只有有一条用例为false，则标记整个大用例模块的测试结果就为false
                                    case_flag = False
                                    break
                        #--------------------------------------------------------------------------------------
                        # 如果存在提交操作
                        if submitAction:
                            self.handCase(submitAction, sheetName, submitName, hightLight)
                        else:
                            logger.info("%s表没有执行%s......" % (funcName, submitName))

                        end_time = datetime.datetime.now()
                        expend_time = end_time - start_time
                        #下面准备写excel操作，需要将excel对象设置为statue=False，即支持读写
                        excel = ParserExcel(pathExcel, statue=False)
                        excel.set_sheet_by_name(funcName)
                        #用例的判断
                        if flag:
                            #记录成功数
                            passNum+=1
                            # 描绘用例模块表---------------------------------------------------------------------------------------------------------
                            # 执行了的用例表格进行颜色描绘
                            for dataAction in Data_Action:
                                if dataAction.get('Colnum'):
                                    excel.set_cellColor(row, dataAction['Colnum'], select_color)
                                else:
                                    break
                            excel.set_cell_value(row, result_colnum, select_words['Success'])
                            excel.set_cellColor(row, result_colnum,select_color)
                            excel.set_cell_value(row, time_colnum,expend_time)
                            excel.set_cellColor(row, time_colnum,select_color)
                            #-----------------------------------------------------------------------------------
                            doc_Explain_name = '用例' + str(orderNum) + '__' + str(explain)+'------'+select_words['Success']
                            #测试用例步骤描述：
                            # testStep = '步骤' +
                            #functionOnce标记funcName，防止重复写入funcName
                            if functionOnce < 1:
                                #有截图
                                if len(capture_pass):
                                    circle_flag = 0
                                    for capturePass in capture_pass:
                                        #只截图一个时
                                        if circle_flag<1:
                                            doc_Explain.append({'funcName': funcName,
                                                                'doc_Explain_name': doc_Explain_name,
                                                                'TestResult_pass': select_words['Success'],
                                                                'TestResult_fail': None,
                                                                'capture_pass': capturePass['capture_name'],
                                                                'capture_fail': None
                                                                })
                                            circle_flag += 1
                                        #一条用例截图两个以上
                                        else:
                                            doc_Explain.append({'funcName': None,
                                                                'doc_Explain_name': None,
                                                                'TestResult_pass': select_words['Success'],
                                                                'TestResult_fail': None,
                                                                'capture_pass': capturePass['capture_name'],
                                                                'capture_fail': None
                                                                })
                                # 没有截图时
                                else:
                                    doc_Explain.append({'funcName': funcName,
                                                        'doc_Explain_name': doc_Explain_name,
                                                        'TestResult_pass': select_words['Success'],
                                                        'TestResult_fail': None,
                                                        'capture_pass': None,
                                                        'capture_fail': None
                                                        })
                                functionOnce += 1
                            else:
                                if len(capture_pass):
                                    circle_flag = 0
                                    for capturePass in capture_pass:
                                        #只截图一个时
                                        if circle_flag<1:
                                            doc_Explain.append({'funcName': None,
                                                                'doc_Explain_name': doc_Explain_name,
                                                                'TestResult_pass': select_words['Success'],
                                                                'TestResult_fail': None,
                                                                'capture_pass': capturePass['capture_name'],
                                                                'capture_fail': None
                                                                })
                                            circle_flag += 1
                                        #一条用例截图两个以上
                                        else:
                                            doc_Explain.append({'funcName': None,
                                                                'doc_Explain_name': None,
                                                                'TestResult_pass': select_words['Success'],
                                                                'TestResult_fail': None,
                                                                'capture_pass': capturePass['capture_name'],
                                                                'capture_fail': None
                                                                })
                                # 没有截图时
                                else:
                                    doc_Explain.append({'funcName': None,
                                                        'doc_Explain_name': doc_Explain_name,
                                                        'TestResult_pass': select_words['Success'],
                                                        'TestResult_fail': None,
                                                        'capture_pass': None,
                                                        'capture_fail': None
                                                        })
                            excel.save_excel()
                            # 关闭excel进程，防止内存占用过大
                            del excel
                            gc.collect()

                        else:
                            #记录失败数
                            failNum += 1
                            # 执行失败的具体表格、异常信息和截图
                            logger.info("错误提示：%s表的第 %s行%s列出现错误，详情可查看log日志或者excel表异常信息" % (funcName, row, Data_Action_Fail[0]))
                            excel.set_cellColor(row, Data_Action_Fail[0], error_color)
                            excel.set_cell_value(row,time_colnum,expend_time)
                            excel.set_cellColor(row,time_colnum,error_color)
                            excel.set_cell_value(row, result_colnum, select_words['Fail'])
                            excel.set_cellColor(row, result_colnum, error_color)
                            excel.set_cell_value(row, exception_colnum, except_message)
                            excel.set_cell_value(row, screent_colnum, result[1])
                            #-------------------------------------------------------------------------------------------------------
                            doc_Explain_name = '用例' + str(orderNum) + '__' + str(explain) + '------' + select_words['Fail']
                            if functionOnce < 1:
                                doc_Explain.append({'funcName': funcName,
                                                    'doc_Explain_name': doc_Explain_name,
                                                    'TestResult_pass': None,
                                                    'TestResult_fail': select_words['Fail'],
                                                    'capture_pass': None,
                                                    'capture_fail': result
                                                    })
                                functionOnce+=1
                            else:
                                doc_Explain.append({'funcName': None,
                                                    'doc_Explain_name': doc_Explain_name,
                                                    'TestResult_pass': None,
                                                    'TestResult_fail': select_words['Fail'],
                                                    'capture_pass': None,
                                                    'capture_fail': result
                                                    })

                            excel.save_excel()
                            # 关闭excel进程，防止内存占用过大
                            del excel
                            gc.collect()
                            #针对用例行的错误处理
                            if select_param:
                                logger.info('遇上失败用例，已开启失败终止模式，终止测试')
                                break
                            else:
                                logger.info('遇上失败用例，已开启失败跳过用例模式，进入下一个用例测试')
                                continue

                # 如果存在后续操作
                if endAction:
                    self.handCase(endAction, sheetName, endName, hightLight)
                else:
                    logger.info("%s表没有执行%s......" % (funcName, endName))

                #-----------------------------------------------------------------------------------------
                excel = ParserExcel(pathExcel, statue=False)
                excel.set_sheet_by_name(case_name['Case'])
                endAll_time = datetime.datetime.now()
                expend_case_time = endAll_time - startAll_time
                #模块的判断
                if case_flag:
                    # 储存用例表执行痕迹
                    excel.set_cellColor(rownum,1,select_color)
                    excel.set_cell_value(rownum,alltime_colnum,expend_case_time)
                    excel.set_cellColor(rownum, alltime_colnum, select_color)
                    excel.set_cell_value(rownum, allresult_colnum, select_words['Success'])
                    excel.set_cellColor(rownum, allresult_colnum, select_color)
                    excel.save_excel()
                    # 关闭excel进程，防止内存占用过大
                    del excel
                    gc.collect()
                else:
                    excel.set_cellColor(rownum, 1, error_color)
                    excel.set_cell_value(rownum, alltime_colnum, expend_case_time)
                    excel.set_cellColor(rownum, alltime_colnum, error_color)
                    excel.set_cell_value(rownum, allresult_colnum, select_words['Fail'])
                    excel.set_cellColor(rownum, allresult_colnum, error_color)
                    excel.save_excel()
                    # 关闭excel进程，防止内存占用过大
                    del excel
                    gc.collect()
                    #以用例行最后一行的失败与成功来判断，如果开启失败跳过，最后一行成功了，再根据判断用例块是否继续
                    if flag:
                        logger.info('前一个模块最后一个用例执行成功，且开启遇上失败继续执行测试...')
                        continue
                    #如果最后一条用例都失败了，那么无论开启跳过与否，都终止执行
                    else:
                        logger.info('前一个模块最后一个用例执行失败，彻底终止测试...')
                        break
            else:
                continue
        logger.info('生成excel测试记录...')
        #下面用于word
        endSum_time = datetime.datetime.now()
        expend_Sumcase_time = endSum_time - startSum_time
        # 测试数目
        funCase_sum = passNum + failNum
        #用于写入word文档
        case_sumNum = []
        case_sumNum.append(funCase_sum)
        case_sumNum.append(passNum)
        case_sumNum.append(failNum)
        case_sumNum.append(expend_Sumcase_time)
        case_sumNum.append(browserValue)
        case_sumNum.append(startTestime)
        #生成word报告文档----------------------------------------------------------------------
        if hightLight is False:
           #生成word报告
           reportName = self.writeDocReport(pathExcel, case_sumNum, doc_Explain)
           #发送邮件
           if switchEmail=='Y':
               logger.info('已开启发送邮件...')
               self.send_Mail(reportName[0],reportName[1])
           else:
               logger.info('已关闭发送邮件...')
        else:
            logger.info('调试模式下不生成word测试报告和不发送邮件...')



