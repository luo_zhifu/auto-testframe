#coding=utf-8
import os
from win32com.client import Dispatch
path_project=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# print(path_project)

#数据管理平台自动化用例位置
nanHaiKeyProjectCase = os.path.join(path_project,r'testData\南海区重点项目平台自动化用例.xlsx')

path_word=os.path.join(path_project,r'testData\测试文档.docx')
#autoit插件驱动文件
exeActionFile = os.path.join(path_project,r'driver\seleniumUploadFile.exe')
testFileload = os.path.join(path_project,r'driver\uploadFile.exe')

path_photo = os.path.join(path_project,r'testData\052201.jpg')
#print(constant.path_excel)
right_picture_path=os.path.join(path_project,r'screenshots\CapturePicture')
error_picture_path=os.path.join(path_project,r'screenshots\ErrorPicture')
log_path = os.path.join(path_project,'logs')
