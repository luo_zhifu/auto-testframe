import time
import datetime
def strftime():
    return time.strftime('%Y-%m-%d/%H:%M:%S',time.localtime())
def strftime_02():
    return time.strftime('%Y/%m/%d_%H-%M-%S',time.localtime())
def strftime_ymd():
    return time.strftime('%Y-%m-%d',time.localtime())
def strftime_hms():
    return time.strftime('%H-%M-%S',time.localtime())

def strftime_HMS():
    return time.strftime('%H:%M:%S',time.localtime())


# starttime = datetime.datetime.now()
#
# for a in range(1,10):
#     for b in range(1,10):
#         print("%d*%d=%2d" % (a,b,a * b), end=" ")
#     print("")
#     time.sleep(2)
# endtime = datetime.datetime.now()
# print((endtime-starttime).seconds)
