import xlwings as xw

class ParserExcel_xlwing(object):
    def __init__(self,file_path):
        self.path = file_path
        self.app = xw.App(visible=False, add_book=False)
        self.app.display_alerts = False
        self.app.screen_updating = False
        self.wb = self.app.books.open(self.path)
    # 保存
    def save_excel(self):
        self.wb.save()
    # 退出、关闭
    def close_excel(self):
        # self.wb.close()
        self.app.kill()