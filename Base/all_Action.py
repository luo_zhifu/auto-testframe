#coding=utf-8
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from Base.var import *
from selenium.webdriver.common.keys import Keys
import os.path
from Base.logger import Logger
from Base.create_dir import *
from Base.parser_time import*
from Base.uploadfile import UploadFille

import xlrd
import os
from xlutils.copy import copy
from xlwt import Style

from win32com.client import Dispatch
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import * #导入所有的异常类
from selenium.webdriver.support import expected_conditions as EC


logger = Logger(logger='Action').getlog()
class Action(object):

    def __init__(self,driver):
        self.driver = driver
    #寻找单个元素
    def find_element(self,*data):
        # logger.info("通过%s方式寻找元素：%s..." % (data[0],data[1]))
        WebDriverWait(self.driver,10).until(lambda x:x.find_element(by=data[0],value=data[1]).is_displayed())
        return self.driver.find_element(data[0],data[1])
    #查找多个元素
    def find_elements(self,*data):
        #find_elements不适用until.......否则会报错
        WebDriverWait(self.driver,10)
        return self.driver.find_elements(data[0],data[1])
    def wait(self,*data):
        logger.info("隐式等待 %d 秒" % int(data[1]))
        self.driver.implicitly_wait(int(data[1]))
    #显式等待
    def driverWait(self,*data):
        WebDriverWait(self.driver,data[1])
    #文件上传方法
    def uploadFile(self,*data):
        dataLen = len(data)
        uploadFile = UploadFille()
        if dataLen > 1:
            uploadFile.uploadfile(data[1])
        else:
            uploadFile.uploadfile(data[0])
    def testupload(self,*data):
        print(testFileload)
        os.system(testFileload)
    def quit_browser(self):
        logger.info("关闭浏览器")
        self.driver.quit()
    #单击
    def click(self, *data):
        logger.info('点击元素 by %s: %s...' % (data[0], data[1]))
        self.find_element(data[0], data[1]).click()
        time.sleep(1)
    #输入值
    def send_key(self,*data):
        logger.info('输入文本: by %s: %s...' % (data[0], data[1]))
        self.find_element(data[0],data[1]).clear()
        self.find_element(data[0],data[1]).send_keys(data[2])
    #根据表格的行、列坐标来定位。主要针对资源目录的信息项填写的方法封装。输入操作
    def sendKeyBytable(self,*data):
        logger.info('根据表格的所在行列坐标输入文本: by %s: %s...' % (data[0], data[1]))
        dataTuple = data[1].split(",")
        trNum = dataTuple[0]
        tdNum = dataTuple[1]
        value = dataTuple[2]
        newElement ="//tr["+trNum+"]//td["+tdNum+"]//input[1]"
        logger.info('根据表格的所在行列坐标定位，定位后输入文本: by %s: %s...' % (data[0], newElement))
        self.find_element(data[0],newElement).clear()
        self.find_element(data[0],newElement).send_keys(value)
    #根据表格的行、列坐标来定位。主要针对资源目录的信息项填写的方法封装，点击操作
    def clickBytable(self,*data):
        logger.info('根据表格的所在行列坐标输入文本: by %s: %s...' % (data[0], data[1]))
        dataTuple = data[1].split(",")
        trNum = dataTuple[0]
        tdNum = dataTuple[1]
        newElement = "//tr["+trNum+"]//td["+tdNum+"]//input[1]/.."
        logger.info('根据表格的所在行列坐标定位，定位后点击目标: by %s: %s...' % (data[0], newElement))
        self.find_element(data[0],newElement).click()

    #对下载的excel附件进行写入数据的操作的方法。后台运行写入的方式（要注意xls、xlsx格式的支持）
    def writeDataToDownloadFile(self,*data):
        rb = xlrd.open_workbook(data[0], formatting_info=True)
        wb = copy(rb)
        #通过sheet的位置
        ws = wb.get_sheet(0)
        dataTuple = data[1].split(";\n")
        rowLength = len(dataTuple)
        neweTupeData = dataTuple[0].split(",")
        colLength = len(neweTupeData)
        # logger.info(f"后台开始进行对excel文件：{data[0]}的第一个sheet写入操作...........")
        for rowNum in range(2,rowLength+2):
            for columnNum in range(1,colLength+1):
                strData=dataTuple[rowNum-2]
                listData = strData.split(",")
                value = listData[columnNum-1]
                styl=Style.default_style
                logger.info(f"正在给文件的第{rowNum-1}行第{columnNum-1}列写入数据{value}")
                ws.write(rowNum-1,columnNum-1,value,styl)
        wb.save(data[0])
        wb.close()
        logger.info("已完成下载文件后的写入操作，保存文件成功.............")
    #方法扩展，针对做同步任务，如果任务不执行成功就每隔5秒点击刷新一次,循环刷新3次的操作
    def refreshTask(self,*data):
        logger.info(f"靓仔！给你介绍元素，要不要：{data[1]}")
        try:
            self.find_element(data[0], data[1])
            logger.info("爽过拜堂！稳到元素啦")
        except Exception:
            for i in range(3):
                time.sleep(5)
                logger.info("不好意思！没找到元素，点击刷新操作...")
                self.find_element(data[0], "//span[contains(text(),'刷新')]").click()

    #清空文本
    def clearValue(self,*data):
        logger.info('清空文本: by %s: %s...' % (data[0], data[1]))
        self.find_element(data[0], data[1]).clear()
    #执行Python语句方法
    def exe_python(self,*data):
        if data[1]:
            logger.info("执行python代码...%s" % data[1])
            exec(data[1])
    #滑动滚动条
    def slipScoll(self,*data):
        logger.info(f"拖动滚动条：{data[0]}像素")
        js="var action=document.documentElement.scrollTop="+data[0]
        self.driver.execute_script(js)
    #js方式点击元素
    def Jsclick(self, *data):
        dataLen = len(data)
        if dataLen > 1:
            elementObj = self.find_element(data[0], data[1])
            logger.info('JS方式点击元素:by %s: %s...' % (data[0], data[1]))
            js = 'arguments[0].click()'
            self.driver.execute_script(js, elementObj)
    #双击
    def DBclick(self,*data):
        logger.info('双击元素 by %s: %s...' % (data[0], data[1]))
        element = self.find_element(data[0], data[1])
        ActionChains(self.driver).double_click(element).perform()
        time.sleep(1)
    #键盘输入
    def keySend(self,*data):
        logger.info('键盘输入内容: %s' % data[1])
        ActionChains(self.driver).send_keys(data[1]).perform()
    #link直接导航
    def getUrl(self,*data):
        dataLen = len(data)
        if dataLen>1:
            logger.info('link导航为：%s' % data[1])
            self.driver.get(data[1])
        else:
            logger.info('link导航为：%s' % data[0])
            self.driver.get(data[0])
    #键盘按键输入
    def keySend_enter(self,*data):
        logger.info('键盘enter')
        ActionChains(self.driver).send_keys(Keys.ENTER).perform()
    #页面值选择
    def selectValue(self,*data):
        for i in data[1]:
            logger.info(f"定位元素：{i}")
            self.Jsclick(data[0],i)

    #针对没有表，然后去生成目标表的特有方法
    def haveTableOrNot(self,*data):
        try:
            logger.info('靓仔！带你去找对象咯 by %s: %s...' % (data[0], data[1]))
            self.find_element(data[0], data[1])
            time.sleep(1)
            #newExpression = local_expression + "'" + str(paramValue)+"')]/../following-sibling::td[last()]//button"
            #如果找到就选择
            selectElement = data[1]+"/../following-sibling::td[last()]//span"
            self.find_element(data[0],selectElement).click()
            logger.info('找到目标对象啦')
        except Exception:
            #找不到就先关闭，然后去生成表
            logger.info('I am so sad! 找不到目标表啊，开始点击生成目标表操作')
            self.find_element("xpath","//span[contains(text(),'选择数据表')]/../button").click()
            self.find_element("xpath","//span[contains(text(),'若无目标表，请点击生成')]").click()
            self.find_element("xpath","//span[contains(text(),'确认')]").click()
            time.sleep(5)
            self.find_element("xpath","//span[contains(text(),'确定')]").click()
            logger.info('生成目标表成功')
    #鼠标移动
    def move_to_element(self,*data):
        #鼠标移动操作
        logger.info('鼠标移动到元素: by %s: %s...' % (data[0], data[1]))
        element = self.find_element(data[0], data[1])
        ActionChains(self.driver).move_to_element(element).perform()
    #鼠标移动
    def moveToElbyLocateAndClick(self,*data):
        #鼠标移动操作
        logger.info('鼠标移动到元素后点击操作: by %s: %s...' % (data[0], data[1]))
        element = self.find_element(data[0], data[1])
        dataTuple = data[2].split(",")
        x = int(dataTuple[0])
        y = int(dataTuple[1])
        ActionChains(self.driver).move_to_element(element).move_by_offset(x,y).click().perform()

    def moveToByLocate(self,*data):
        #鼠标移动操作
        logger.info('鼠标从起始元素移动到目标元素: by %s: %s...' % (data[0], data[1]))
        element = self.find_element(data[0], data[1])
        dataTuple = data[2].split(",")
        x = int(dataTuple[0])
        y = int(dataTuple[1])
        x1 = int(dataTuple[2])
        y1 = int(dataTuple[3])
        ActionChains(self.driver).move_to_element(element).move_by_offset(x,y).\
            click_and_hold().move_by_offset(x1,y1).release().perform()

    #鼠标拖拉操作
    def chainsMoveAction(self,*data):
        #鼠标拖拉操作
        logger.info('鼠标拖拉元素: by %s: %s...' % (data[0], data[1]))
        element = self.find_element(data[0], data[1])
        dataTuple = data[2].split(",")
        x = int(dataTuple[0])
        y = int(dataTuple[1])
        ActionChains(self.driver).drag_and_drop_by_offset(element,x,y).release().perform()

    #右击后键盘按键
    def context_key(self,*data):
        logger.info('右击元素 by %s: %s...' % (data[0], data[1]))
        logger.info('键盘输入内容: %s' % data[2])
        element = self.find_element(data[0], data[1])
        ActionChains(self.driver).context_click(element).perform()
        ActionChains(self.driver).send_keys(data[2]).perform()
    #js去除元素属性的方法
    def removeAttribute(self,*data):
        '''执行js'''
        dataLen = len(data)
        if dataLen > 1:
            logger.info("操作删除元素的属性:%s..." % data[2])
            elementObj = self.find_element(data[0], data[1])
            js = 'arguments[0].removeAttribute(arguments[1])'
            self.driver.execute_script(js,elementObj,data[2])

    def get_text(self, *data):
        '''获取文本'''
        element = self.find_element(data[0], data[1])
        return element.text
    def get_page_title(self):
        logger.info("当前页面的title为: %s" % self.driver.title)
        return self.driver.title

    #主动截图
    def capture_screen(self,*data):
        path = create_day_dir(right_picture_path)
        # 防止每次相同地方，出现重复截图
        file_name = os.path.join(path,str(data[1])+'_'+data[0]+'.png')
        logger.info("主动使用截图...")
        self.driver.get_screenshot_as_file(file_name)
        screen_name = str(data[1])+'_'+data[0]+'.png'
        return file_name,screen_name
    # 错误截图，已封装发生错误时自动截图的该方法调用
    def capture_error_screen(self,*errorData):
        path = create_day_dir(error_picture_path)
        #防止每次相同地方报错，出现重复截图
        errorPictureName = errorData[0]+'_测试用例第'+errorData[1]+'行'+errorData[2]+'列'+'_'+errorData[3]+'操作执行失败'
        errorPictureNameDoc = '测试'+errorData[0] + '的功能：'+ errorData[3]+'_步骤失败'
        file_name = os.path.join(path, errorPictureName + '.png')
        logger.info("错误页面截图...")
        self.driver.get_screenshot_as_file(file_name)
        screen_name = errorPictureName+ '.png'
        screenNameDoc = errorPictureNameDoc
        return file_name,screen_name,screenNameDoc
    #拼接元素的高亮
    def ele_highLightSpilt(self,*data):
        dataLen = len(data)
        if dataLen>1:
            for expression in data[1]:

                elementObj = self.find_element(data[0], expression)
                logger.info("高亮元素: %s" % expression)
                js = "arguments[0].style.border='2px solid red'"
                js_02 = "arguments[0].style.border='2px solid white'"
                #模拟高亮时闪的形式
                for i in range(1,3):
                    self.driver.execute_script(js, elementObj)
                    time.sleep(0.1)
                    self.driver.execute_script(js_02, elementObj)
                    time.sleep(0.1)
                    self.driver.execute_script(js, elementObj)
                    time.sleep(0.1)

                self.click(data[0],expression)
        else:
            pass
        #self.driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", elementObj,
        #"background:green; border:2px solid red;")
    #一般元素的高亮
    def ele_highLight(self, *data):
        dataLen = len(data)
        if dataLen > 1:
            elementObj = self.find_element(data[0], data[1])
            logger.info('高亮元素: by %s: %s...' % (data[0], data[1]))
            js = "arguments[0].style.border='2px solid red'"
            js_02 = "arguments[0].style.border='2px solid white'"
            # 模拟高亮时闪的形式
            for i in range(1, 3):
                self.driver.execute_script(js, elementObj)
                time.sleep(0.1)
                self.driver.execute_script(js_02, elementObj)
                time.sleep(0.1)
                self.driver.execute_script(js, elementObj)
                time.sleep(0.1)
        else:
            pass
    # 两个窗口时，删除第二个窗口后，回到当前窗口
    def delSecondWin(self,*data):
        cut_window = self.driver.current_window_handle
        logger.info("当前窗口句柄为：%s" % cut_window)
        all_handles = self.driver.window_handles
        logger.info("所有窗口句柄为：%s" % all_handles)
        list = []
        for i in all_handles:
            if i not in cut_window:
                list.append(i)
                self.driver.switch_to.window(list[0])
                logger.info("删除的窗口句柄为: %s" % i)
                self.driver.close()
        logger.info("删除后回到窗口: %s" % cut_window)
        self.driver.switch_to.window(cut_window)
        # self.driver.maximize_window()
    # 两个窗口时，去到非當前的窗口
    def switchToSecondWin(self,*data):
        cut_window = self.driver.current_window_handle
        logger.info("当前窗口句柄为：%s" % cut_window)
        all_handles = self.driver.window_handles
        logger.info("所有窗口句柄为：%s" % all_handles)
        list = []
        for i in all_handles:
            if i not in cut_window:
                list.append(i)
                self.driver.switch_to.window(list[0])
                logger.info("去到非当前窗口为: %s" % i)
    #断言
    def assert_word(self,*data):
        try:
            logger.info('断言数据---by %s: %s...' % (data[0], data[1]))
            keyword =self.find_element(data[0], data[1]).text
            assert str(data[2]) in str(keyword)
            logger.info("断言成功...")
        except:
            logger.info('断言失败...')
            raise
