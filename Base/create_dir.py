import os
from Base.parser_time import strftime_ymd
def create_day_dir(path):
    path=os.path.join(path,strftime_ymd())
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
    return path
def create_capture_dir(path):
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
    return path
if __name__=='__main__':
    from Base.var import right_picture_path
    create_day_dir(right_picture_path)
