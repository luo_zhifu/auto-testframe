from Base.var import *
from openpyxl import load_workbook
import openpyxl.styles as sty
from openpyxl.styles import Color,Fill
import gc

class ParserExcel(object):
    def __init__(self,path,statue = True):
        self.path=path
        self.excel=load_workbook(self.path,data_only=statue)
        self.sheet=self.excel.active

    def set_sheet_by_index(self,index):
        self.sheet=self.get_sheet_by_index(index)
    def set_sheet_by_name(self,name):
        self.sheet=self.excel[name]
    def get_sheet_by_index(self,index):
        return self.excel[self.get_all_sheet_names()[index-1]]
    def get_all_sheet_names(self):
        return self.excel.sheetnames
    def get_all_rows(self):
        rows=[]
        for row in self.sheet.rows:
            rows.append(row)
        return rows
    def get_all_rowsvalue(self):
        rowsvalue=[]
        for row in self.get_all_rows():
            rowvalue=[]
            for cell in row:
                rowvalue.append(cell.value)
            rowsvalue.append(rowvalue)
        return rowsvalue
    def get_all_cols(self):
        cols=[]
        for col in self.sheet.columns:
            cols.append(col)
        return cols
    def get_all_colsvalue(self):
        colsvalue=[]
        for col in self.get_all_cols():
            colvalue=[]
            for cell in col:
                colvalue.append(cell.value)
            colsvalue.append(colvalue)
        return colsvalue
    def get_max_row(self):
        return self.sheet.max_row
    def get_max_column(self):
        return self.sheet.max_column
    def get_rows(self):
        return self.sheet.rows
    def get_cols(self):
        return self.sheet.columns
    def get_cell_value(self,rownum,colnum):
        return self.sheet.cell(rownum,colnum).value

    def get_cell(self,rownum,colnum):
        return self.sheet.cell(rownum,colnum)
    def set_cell_value(self,rownum,colnum,value):
        self.sheet.cell(rownum,colnum,value=value)
    def save_excel(self):
        self.excel.save(self.path)
    def del_excel(self):
        # 关闭excel进程，防止内存占用过大
        del excel
        gc.collect()
    #设置单元格颜色
    def set_cellColor(self,rownum,colnum,color):
        self.sheet.cell(rownum, colnum).fill = sty.PatternFill(fill_type='solid', fgColor=color)
if __name__=="__main__":
    excel=ParserExcel(path_excel_02)
    # excel.set_sheet_by_name('菜单选择')
    excel.set_sheet_by_index(2)
    rows = excel.get_cell_value(3,3)
    print(rows)
