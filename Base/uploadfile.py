#coding=utf-8
import os.path
import yaml
from Base.logger import Logger
from ruamel import yaml
from Base.var import *

logger = Logger(logger="UploadFille").getlog()

class UploadFille(object):
    def uploadfile(self,filePath):
        # 读取yaml文件数据
        curpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        yaml_path = os.path.join(curpath, r"yaml\reuseBrowser.yaml")
        readData = open(yaml_path, "r")
        result = yaml.load(readData.read(), Loader=yaml.Loader)
        browser = result['browser']
        # 解决IO警告
        readData.close()
        #根据使用的浏览器去调用对应方法
        os.system(exeActionFile+" "+browser+" "+filePath)
        logger.info("调用autoIT驱动上传文件： %s" % filePath)

